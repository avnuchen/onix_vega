PROGRAM=ONIX_IPP
#MPILAM=  -us -I/usr/local/include #-L/$(HOME)/BLAS-3.6.0/ -lblas
ACCELERATOR=-O3 
#-axT #-L$(HOME)/mpiP-3.4.1/ -lm -L $(HOME)/libunwind/lib64 -lunwind -lmpiPi
FFLAGS = -w -ffree-line-length-none
#NUMPROCESS=12
COMPILER = mpif90
FILES = $(wildcard *.in) $(wildcard *H*) $(wildcard *LAG*) Proces.dat $(PROGRAM) $(wildcard *.f90) B3 B9 
SRCS = ONIX_IPP.f90 $(wildcard *.f90)
OBJS = $(SRCS:.f90=.o)
RUNFILE = $(shell date +'%Y.%m.%d-%H:%M:%S')


all: $(PROGRAM)
	
#@echo "------------------COMPILATION--------------------check bounds"
ONIX_IPP: $(OBJS)
	$(COMPILER) $(FFLAGS) -o $@ $^ $(MPILAM) $(ACCELERATOR)

%.o: %.f90
	$(COMPILER) $(FFLAGS) -c $< $(MPILAM) $(ACCELERATOR)


run:
	@echo "------------------RUN MPI PROGRAM---------------" 
	mkdir $(RUNFILE)
	cp $(FILES) $(RUNFILE)/
	cd $(RUNFILE); \
	nohup mpirun -np $(NUMPROCESS) --mca btl tcp,sm,self ./$(PROGRAM)>results&
	#mpirun -np $(NUMPROCESS) -mca btl tcp,sm,self ./$(PROGRAM)
	
timerun:
	@echo "------------------TIME CHECK RUN MPI PROGRAM---------------"
	time mpirun-lam -np $(NUMPROCESS) $(PROGRAM)  -check bounds

clean:
	@echo "------------------EREASE ALL FORT.* FILES---------------"
	rm -f fort.*
	rm -f Wall*.*
	rm -f noh*
	rm -f resul*
	rm -f ONIX
	rm -f $(PROGRAM)
	rm -f $(OBJS)
