#!/usr/bin/bash
#SBATCH --time=00:05:00
#SBATCH --sockets-per-node=8
#SBATCH --cores-per-socket=16
#SBATCH --threads-per-core=2
#SBATCH --hint=nomultithread
#SBATCH --ntasks-per-core=1
#SBATCH --partition=cpu
#SBATCH -n 4096
#SBATCH --job-name=ONIX_job
#SBATCH --error  myJob_ONIX_%j.err      # std-error file
#SBATCH --output myJob_ONIX_%j.out      # std-output file  

##SBATCH --mail-type=ALL 
##SBATCH --mail-user=anna.vnuchenko@cern.ch
##SBATCH --exclusive 
                                                                                 
ulimit -s unlimited

#module load mpi/mvapich2/2.2
module load openmpi/gnu/4.0.5.2

FOLDER_NAME=${SLURM_JOB_NAME}_${SLURM_JOBID}
BINARY=ONIX_IPP

mkdir $FOLDER_NAME
cp {$BINARY,submit.sh} $FOLDER_NAME
cp *.dat $FOLDER_NAME
cp *.in $FOLDER_NAME
cp ELISE_Ue $FOLDER_NAME
cp IS03_field* $FOLDER_NAME 
cp *.f90 $FOLDER_NAME
#mv PaPos/* $FOLDER_NAME

cd $FOLDER_NAME
srun --mpi=pmix_v3 ./$BINARY>myoutput

mv ../myJob_ONIX_${SLURM_JOBID}.err .
mv ../myJob_ONIX_${SLURM_JOBID}.out .
