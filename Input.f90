 
    subroutine Read_Parameters()
	use Parallel
	implicit none

	integer, parameter :: input_unit = 10
	integer  file_status
	character (len=20) Text

	open( unit=input_unit, file="Proces.dat", status="old",iostat=file_status)

	if(file_status==0) then

		read( unit=input_unit, fmt=* ) Text
                read( unit=input_unit, fmt=* ) NprocX,NprocY,NprocZ
		read( unit=input_unit, fmt=* ) Text
		read( unit=input_unit, fmt=* ) periodic
		read( unit=input_unit, fmt=* ) Text
		read( unit=input_unit, fmt=* ) Nxp,Nyp,Nzp
		read( unit=input_unit, fmt=* ) Text
		read( unit=input_unit, fmt=* ) time_collision
		read( unit=input_unit, fmt=* ) Text
		read( unit=input_unit, fmt=* ) dt
    read( unit=input_unit, fmt=* ) Text
    read( unit=input_unit, fmt=* ) Iteration_Number
    read( unit=input_unit, fmt=* ) Text
    read( unit=input_unit, fmt=* ) NI_emission
    read( unit=input_unit, fmt=* ) Text
    read( unit=input_unit, fmt=* ) delta(0), delta(1), delta(2)
    read( unit=input_unit, fmt=* ) Text
    read( unit=input_unit, fmt=* ) plasma_x0, plasma_x1
    read( unit=input_unit, fmt=* ) Text
    read( unit=input_unit, fmt=* ) filenameB
    read( unit=input_unit, fmt=* ) NxB, NyB, NzB, deltaxB, deltayB, deltazB

		close( unit=input_unit )

    write(*,*) filenameB, rank

	else
 		print *, "Unable to open file : Proces.dat"
		
	endif

    end subroutine Read_Parameters
    
    
    subroutine Read_Particles()
	use Parallel
	implicit none

  integer :: l
  logical isin
	integer, parameter :: input_unit = 11
	integer  file_status,i,ttt,counter_i
	
	character (len=7) uuu

	integer part_counter
	
	counter_i=0
	ttt=Iteration_Number+rank

	part_counter=0

	!verify the length of the number in Pqa_pos_NUMBER file and change (i5) (i6) according to the length
	write(uuu,'(i7)') ttt

   lgc=LEN_TRIM(ADJUSTL(uuu))
   open( unit=input_unit, file='Pa_pos_'//uuu(8-lgc:7)//'.dat', status="old",iostat=file_status)

	if(file_status==0) then

		read( unit=input_unit, fmt=* ) Npart

		do i=1,Npart

			read( unit=input_unit, fmt=* ) counter_i, P(i)%X, P(i)%Y, P(i)%Z, P(i)%sp,P(i)%VX,P(i)%VY,P(i)%VZ,P(i)%IdentPart
			!P(i)%IdentPart=0.0
			
			!if( part_counter < 1 ) then
			!	if(  P(i)%sp == 9 .OR. P(i)%sp == 5 .OR. P(i)%sp == 0 ) then			
			!		if( (P(i)%X > plasma_x1)  .and. (P(i)%X < The_Object(2)%center(0)+The_Object(2)%width(0)) ) then
			!			P(i)%IdentPart=100.0*rank + P(i)%sp
			!			part_counter=part_counter+1
			!		endif
			!	endif			
			!endif
		enddo

		close( unit=input_unit )

	else
 		print *, "ERROR!!!!  Unable to open file : ",'Pa_pos_'//uuu(8-lgc:7)//'.dat'
        stop
		
	endif
  
  if(3==3) then
  OutParticles = 0
  do l=1, Npart
    isin = .FALSE.
    do ll=1, Nb_Obj
      call isinobj(P(l)%x, P(l)%y, P(l)%z, ll, isin)
      if(isin .EQV. .TRUE.) then
        OutParticles = OutParticles + 1
        DelParticles(OutParticles) = l
        exit
      end if
    end do
  end do

  call Heapsort(DelParticles, OutParticles)
  call DeleteParticle()
  end if


    end subroutine Read_Particles
    
 
    
    
    subroutine Read_Bfield_IPP()
	use Parallel
	implicit none

	integer, parameter :: input_unit = 11
	integer  file_status,h1,h2,h3,i,j,k

	real referBx,referBy,referBz
	character (len=40) FileName

    
    
    FileName="LAG_2x4_9_SideApp_2.txt"
	h1=0
	h2=0
	h3=0
	i=0
	j=0
	k=0
	referBx=-100
	referBy=-100
	referBz=-100
						   
	open( unit=input_unit, file=FileName, status="old",iostat=file_status)

	if(file_status==0) then
 		do h1=1, 453690
		

     		read( unit=input_unit, fmt=* ) ReadB(h1,1),ReadB(h1,2),ReadB(h1,3),ReadB(h1,4),ReadB(h1,5),ReadB(h1,6),ReadB(h1,7)
		
		enddo

		close( unit=input_unit )

	else
 		print *, "Unable to open file : ",FileName
        stop
		
    endif
    



	do i=i0-1, i1+1
 			do j=j0-1, j1+1
 				do k=k0-1, k1+1
 					
			       		do h2=1, 453690                          
                                
                                
                                if(abs(i*delta(0)-ReadB(h2,1)*1e-3)<1e-5  .AND.   abs(j*delta(1)-ReadB(h2,2)*1e-3)<1e-5  .AND.   abs(k*delta(2)-ReadB(h2,3)*1e-3)<1e-5  )  then                           
                                        
                                       ! write(900+rank,*) i,j,k,h2,ReadB(h2,4)*0.0001
                                          Bxm(i,j,k)=ReadB(h2,4)*0.0001
						                  Bym(i,j,k)=ReadB(h2,5)*0.0001
						                  Bzm(i,j,k)=ReadB(h2,6)*0.0001
                                        
                                endif
							
					   enddo

						    !write(rank+580,'(3(1x,i8) ,6(1x,8e) )') i,j,k,Bxm(i,j,k),Bym(i,j,k),Bzm(i,j,k)



 				enddo
 			enddo			
 		enddo

     




     
     
    end subroutine Read_Bfield_IPP
    
    
    
    
    
subroutine Read_Bfield_IPP2()
	use Parallel
	implicit none

	integer, parameter :: input_unit = 11
	integer  file_status,h1,h2,h3,i,j,k

	real referBx,referBy,referBz
	character (len=40) FileName,test1

    
              
    FileName="LAG_2x4_9_CentralApp_corr.txt"
	h1=0
	h2=0
	h3=0
	i=0
	j=0
	k=0
	referBx=-100
	referBy=-100
	referBz=-100
	  
	open( unit=input_unit, file=FileName, status="old",iostat=file_status)
 
   	if(file_status==0) then
  
        read( unit=input_unit, fmt=* ) test1
        
 		do h1=1, 453690
		
     		read( unit=input_unit, fmt=* ) ReadB(h1,1),ReadB(h1,2),ReadB(h1,3),ReadB(h1,4),ReadB(h1,5),ReadB(h1,6),ReadB(h1,7)
	
		enddo

		close( unit=input_unit )

    else
   
 		print *, "Unable to open file : ",FileName

    endif

	do i=i0-1, i1+1
 			do j=j0-1, j1+1
 				do k=k0-1, k1+1
 					
			       		do h2=1, 453690                          
                                
                                
                                if(abs(i*delta(0)-ReadB(h2,1)*1e-3)<0.00017  .AND.   abs(j*delta(1)-ReadB(h2,2)*1e-3)<1e-5  .AND.   abs(k*delta(2)-ReadB(h2,3)*1e-3)<1e-5  )  then                           
                                        
                                       ! write(900+rank,*) i,j,k,h2,ReadB(h2,4)*0.0001
                                         Bxm(i,j,k)=ReadB(h2,4)*0.0001
						                 Bym(i,j,k)=ReadB(h2,5)*0.0001
						                 Bzm(i,j,k)=ReadB(h2,6)*0.0001
                                        
                                endif
							
					   enddo

						    !write(rank+580,'(3(1x,i8) ,6(1x,8e) )') i,j,k,Bxm(i,j,k),Bym(i,j,k),Bzm(i,j,k)



 				enddo
 			enddo			
 		enddo

     




     
     
end subroutine Read_Bfield_IPP2
subroutine Read_Bfield()
	use Parallel
	implicit none

	integer, parameter :: input_unit = 11
	integer  file_status,h1,h2,h3,i,j,k

	real referBx,referBy,referBz
	

	h1=0
	h2=0
	h3=0
	i=0
	j=0
	k=0
	referBx=-100
	referBy=-100
	referBz=-100
						   

	open( unit=input_unit, file="Linac4_IS02.table", status="old",iostat=file_status)

	if(file_status==0) then
 		do h1=1, 1449981
		

     		read( unit=input_unit, fmt=* ) ReadB(h1,1),ReadB(h1,2),ReadB(h1,3),ReadB(h1,4),ReadB(h1,5),ReadB(h1,6)
		

		enddo


		close( unit=input_unit )

	else
 		print *, "Unable to open file : Linac4_IS02.table"
		
	endif


 call cpu_time ( time1 )
write(6,*) "BEGIN ",  time1


	do i=i0, i1
 			do j=j0, j1
 				do k=k0, k1
 					
			       		do h2=1, 1449981

						  if( ((ReadB(h2,3)-11.59)*0.01 <(i-3)*delta(0) ) .AND. ((ReadB(h2+1,3)-11.59)*0.01>= (i-3)*delta(0) )  ) referBx=ReadB(h2,3)
						  if( ((ReadB(h2,1)+1.0)*0.01<(j-2)*delta(1) )    .AND. ((ReadB(h2+1,1)+1.0)*0.01>= (j-2)*delta(1) )  ) referBy=ReadB(h2,1)+1.0
						  if( ((ReadB(h2,2)+1.0)*0.01<(k-2)*delta(2))     .AND. ((ReadB(h2+1,2)+1.0)*0.01>= (k-2)*delta(2))   ) referBz=ReadB(h2,2)+1.0
						

				                     if (mod (h2,1000000) .EQ. 0  .AND. (rank==0) ) then

								  call cpu_time ( time2 )
								   write(6,*) "mmm=",i,j,k,time2
   
						     endif
					 enddo

					 do h3=1, 1449981

					    if( abs(ReadB(h3,1)+1.0 - (referBy) )<1e-5  .AND.  abs(ReadB(h3,2)+1.0 - (referBz) )<1e-5 .AND.  abs(ReadB(h3,3)- referBx)<1e-5      ) then


						    Bxm(i,j,k)=ReadB(h3,6)*0.0001
						    Bym(i,j,k)=ReadB(h3,4)*0.0001
						    Bzm(i,j,k)=ReadB(h3,5)*0.0001


						    referBx=-100
						    referBy=-100
						    referBz=-100
						   



					     endif



					 enddo


						    write(rank+680,'(3(1x,i8) ,6(1x,E11.4) )') i,j,k,Bxm(i,j,k),Bym(i,j,k),Bzm(i,j,k)



 				enddo
 			enddo			
 		enddo



end subroutine Read_Bfield





subroutine Read_Bfield_after_interpolation_IS01()
	use Parallel
	implicit none

	integer, parameter :: input_unit = 11
	integer  file_status,i,j,k,ttt,counter_i,testX,testY,testZ
	
	character (len=3) uuu


	counter_i=0
	ttt=680+rank
	!verify the length of the number in Pqa_pos_NUMBER file and change (i5) (i6) according to the length
	write(uuu,'(i3)') ttt

	open( unit=input_unit, file='bfield_IS01_'//uuu//'.dat', status="old",iostat=file_status)

	if(file_status==0) then

		do i=i0, i1
 			do j=j0, j1
 				do k=k0, k1

					read( unit=input_unit, fmt=* ) testX, testY,testZ,Bxm(i,j,k),Bym(i,j,k),Bzm(i,j,k)


					 ! write(1500+rank,*) i,j,k,Bxm(i,j,k)
					
				enddo
			enddo
		enddo

		close( unit=input_unit )

	else
 		print *, "Unable to open file : bfield_IS01_"
		
	endif




end subroutine Read_Bfield_after_interpolation_IS01

subroutine Read_Bfield_after_interpolation_IS02()
	use Parallel
	implicit none

	integer, parameter :: input_unit = 11
	integer  file_status,i,j,k,ttt,counter_i,testX,testY,testZ
	
	character (len=3) uuu


	counter_i=0
	ttt=680+rank
	!verify the length of the number in Pqa_pos_NUMBER file and change (i5) (i6) according to the length
	write(uuu,'(i3)') ttt

	open( unit=input_unit, file='bfield_IS02_'//uuu//'.dat', status="old",iostat=file_status)

	if(file_status==0) then

		do i=i0, i1
 			do j=j0, j1
 				do k=k0, k1

					read( unit=input_unit, fmt=* ) testX, testY,testZ,Bxm(i,j,k),Bym(i,j,k),Bzm(i,j,k)


					 ! write(1500+rank,*) i,j,k,Bxm(i,j,k)
					
				enddo
			enddo
		enddo

		close( unit=input_unit )

	else
 		print *, "Unable to open file : bfield_IS02_"
		
	endif




end subroutine Read_Bfield_after_interpolation_IS02

subroutine Read_Bfield_IPP3()

  use Parallel
  implicit none

  integer, parameter :: input_unit = 11
  integer  file_status,h1,h2,h3,i,j,k

  character (len=40) FileName
  real a,b,cc,ddd, xmaxB, ymaxB, zmaxB, posx, posy, posz, ddx, ddy, ddz
  
  NxB=108
  NyB=81
  NzB=81

  allocate(BxRead(1:NxB, 1:NyB, 1:NzB))
  allocate(ByRead(1:NxB, 1:NyB, 1:NzB))
  allocate(BzRead(1:NxB, 1:NyB, 1:NzB))
  
  deltaxB=0.25e-3
  deltayB=0.25e-3
  deltazB=0.25e-3
  
  xmaxB = (NxB-1)*deltaxB
  ymaxB = (NyB-1)*deltayB
  zmaxB = (NzB-1)*deltazB
  
  FileName="IS03_field_28mm_11mT"
  
  
  
  h1=0
  h2=0
  h3=0
  i=0
  j=0
  k=0
    
  open(unit=input_unit, file=FileName, status="old",iostat=file_status)
  
  if(file_status==0) then
    !read( unit=input_unit, fmt=* ) test1
    !do k=1,NzB
    !  do j=NyB, 1, -1
    !    do i=1, NxB
    !      read( unit=input_unit, fmt=* ) a,b,cc,BxRead(i,j,k),ByRead(i,j,k),BzRead(i,j,k),ddd
    !    end do
    !  end do
    !end do
    
    do i=1,NxB
      do j=1, NyB
        do k=1, NzB
		  read( unit=input_unit, fmt=* ) a,b,cc,BxRead(i,j,k),ByRead(i,j,k),BzRead(i,j,k)	
			
        end do
      end do
    end do

    
    close( unit=input_unit )

  else
   print *, "Unable to open file : ",FileName
  endif

  !BxRead = BxRead*1e-4
  !ByRead = ByRead*1e-4
  !BzRead = BzRead*1e-4

  do i=i0-1, i1+1
    do j=j0-1, j1+1
      do k=k0-1, k1+1
        posx = i*delta(0)
        posy = j*delta(1)
        posz = k*delta(2)
        if(posx>0. .AND. posx<xmaxB .AND. posy>0. .AND. posy<ymaxB .AND. posz>0. .AND. posz<zmaxB) then
          h1 = posx/deltaxB+1
          h2 = posy/deltayB+1
          h3 = posz/deltazB+1
          dx = posx-(h1-1)*deltaxB
          dy = posy-(h2-1)*deltayB
          dz = posz-(h3-1)*deltazB
          ddx = 1.-dx
          ddy = 1.-dy
          ddz = 1.-dz
          Bxm(i,j,k) = ddx*ddy*ddz*BxRead(h1  ,h2  ,h3  ) &
                     + ddx*ddy* dz*BxRead(h1  ,h2  ,h3+1) &
                     + ddx* dy*ddz*BxRead(h1  ,h2+1,h3  ) &
                     + ddx* dy* dz*BxRead(h1  ,h2+1,h3+1) &
                     +  dx*ddy*ddz*BxRead(h1+1,h2  ,h3  ) &
                     +  dx*ddy* dz*BxRead(h1+1,h2  ,h3+1) &
                     +  dx* dy*ddz*BxRead(h1+1,h2+1,h3  ) &
                     +  dx* dy* dz*BxRead(h1+1,h2+1,h3+1)
          Bym(i,j,k) = ddx*ddy*ddz*ByRead(h1  ,h2  ,h3  ) &
                     + ddx*ddy* dz*ByRead(h1  ,h2  ,h3+1) &
                     + ddx* dy*ddz*ByRead(h1  ,h2+1,h3  ) &
                     + ddx* dy* dz*ByRead(h1  ,h2+1,h3+1) &
                     +  dx*ddy*ddz*ByRead(h1+1,h2  ,h3  ) &
                     +  dx*ddy* dz*ByRead(h1+1,h2  ,h3+1) &
                     +  dx* dy*ddz*ByRead(h1+1,h2+1,h3  ) &
                     +  dx* dy* dz*ByRead(h1+1,h2+1,h3+1)
          Bzm(i,j,k) = ddx*ddy*ddz*BzRead(h1  ,h2  ,h3  ) &
                     + ddx*ddy* dz*BzRead(h1  ,h2  ,h3+1) &
                     + ddx* dy*ddz*BzRead(h1  ,h2+1,h3  ) &
                     + ddx* dy* dz*BzRead(h1  ,h2+1,h3+1) &
                     +  dx*ddy*ddz*BzRead(h1+1,h2  ,h3  ) &
                     +  dx*ddy* dz*BzRead(h1+1,h2  ,h3+1) &
                     +  dx* dy*ddz*BzRead(h1+1,h2+1,h3  ) &
                     +  dx* dy* dz*BzRead(h1+1,h2+1,h3+1)
          !write(200000+rank,*) i, j, k, posx, posy, posz, Bxm(i,j,k), Bym(i,j,k), Bzm(i,j,k)
        end if
!if(3==3) then
        if(posx==0. .AND. posy>0. .AND. posy<ymaxB .AND. posz>0. .AND. posz<zmaxB) then
          h1 = posx/deltaxB+1
          h2 = posy/deltayB+1
          h3 = posz/deltazB+1
          dy = posy-(h2-1)*deltayB
          dz = posz-(h3-1)*deltazB
          ddy = 1.-dy
          ddz = 1.-dz
          Bxm(i,j,k) = ddy*ddz*BxRead(h1  ,h2  ,h3  ) &
                     + ddy* dz*BxRead(h1  ,h2  ,h3+1) &
                     +  dy*ddz*BxRead(h1  ,h2+1,h3  ) &
                     +  dy* dz*BxRead(h1  ,h2+1,h3+1) 
          Bym(i,j,k) = ddy*ddz*ByRead(h1  ,h2  ,h3  ) &
                     + ddy* dz*ByRead(h1  ,h2  ,h3+1) &
                     +  dy*ddz*ByRead(h1  ,h2+1,h3  ) &
                     +  dy* dz*ByRead(h1  ,h2+1,h3+1) 
          Bzm(i,j,k) = ddy*ddz*BzRead(h1  ,h2  ,h3  ) &
                     + ddy* dz*BzRead(h1  ,h2  ,h3+1) &
                     +  dy*ddz*BzRead(h1  ,h2+1,h3  ) &
                     +  dy* dz*BzRead(h1  ,h2+1,h3+1) 

        end if
        if(posx==xmaxB .AND. posy>0. .AND. posy<ymaxB .AND. posz>0. .AND. posz<zmaxB) then
          h1 = posx/deltaxB+1
          h2 = posy/deltayB+1
          h3 = posz/deltazB+1
          dy = posy-(h2-1)*deltayB
          dz = posz-(h3-1)*deltazB
          ddy = 1.-dy
          ddz = 1.-dz
          Bxm(i,j,k) = ddy*ddz*BxRead(h1  ,h2  ,h3  ) &
                     + ddy* dz*BxRead(h1  ,h2  ,h3+1) &
                     +  dy*ddz*BxRead(h1  ,h2+1,h3  ) &
                     +  dy* dz*BxRead(h1  ,h2+1,h3+1) 
          Bym(i,j,k) = ddy*ddz*ByRead(h1  ,h2  ,h3  ) &
                     + ddy* dz*ByRead(h1  ,h2  ,h3+1) &
                     +  dy*ddz*ByRead(h1  ,h2+1,h3  ) &
                     +  dy* dz*ByRead(h1  ,h2+1,h3+1) 
          Bzm(i,j,k) = ddy*ddz*BzRead(h1  ,h2  ,h3  ) &
                     + ddy* dz*BzRead(h1  ,h2  ,h3+1) &
                     +  dy*ddz*BzRead(h1  ,h2+1,h3  ) &
                     +  dy* dz*BzRead(h1  ,h2+1,h3+1) 

        end if
        if(posx>0 .AND. posx<xmaxB .AND. posy==0. .AND. posz>0. .AND. posz<zmaxB) then
          h1 = posx/deltaxB+1
          h2 = posy/deltayB+1
          h3 = posz/deltazB+1
          dx = posx-(h1-1)*deltaxB
          dz = posz-(h3-1)*deltazB
          ddx = 1.-dx
          ddz = 1.-dz
          Bxm(i,j,k) = ddx*ddz*BxRead(h1  ,h2  ,h3  ) &
                     + ddx* dz*BxRead(h1  ,h2  ,h3+1) &
                     +  dx*ddz*BxRead(h1+1,h2  ,h3  ) &
                     +  dx* dz*BxRead(h1+1,h2  ,h3+1) 
          Bym(i,j,k) = ddx*ddz*ByRead(h1  ,h2  ,h3  ) &
                     + ddx* dz*ByRead(h1  ,h2  ,h3+1) &
                     +  dx*ddz*ByRead(h1+1,h2  ,h3  ) &
                     +  dx* dz*ByRead(h1+1,h2  ,h3+1) 
          Bzm(i,j,k) = ddx*ddz*BzRead(h1  ,h2  ,h3  ) &
                     + ddx* dz*BzRead(h1  ,h2  ,h3+1) &
                     +  dx*ddz*BzRead(h1+1,h2  ,h3  ) &
                     +  dx* dz*BzRead(h1+1,h2  ,h3+1) 
        end if
        if(posx>0 .AND. posx<xmaxB .AND. posy==ymaxB .AND. posz>0. .AND. posz<zmaxB) then
          h1 = posx/deltaxB+1
          h2 = posy/deltayB+1
          h3 = posz/deltazB+1
          dx = posx-(h1-1)*deltaxB
          dz = posz-(h3-1)*deltazB
          ddx = 1.-dx
          ddz = 1.-dz
          Bxm(i,j,k) = ddx*ddz*BxRead(h1  ,h2  ,h3  ) &
                     + ddx* dz*BxRead(h1  ,h2  ,h3+1) &
                     +  dx*ddz*BxRead(h1+1,h2  ,h3  ) &
                     +  dx* dz*BxRead(h1+1,h2  ,h3+1) 
          Bym(i,j,k) = ddx*ddz*ByRead(h1  ,h2  ,h3  ) &
                     + ddx* dz*ByRead(h1  ,h2  ,h3+1) &
                     +  dx*ddz*ByRead(h1+1,h2  ,h3  ) &
                     +  dx* dz*ByRead(h1+1,h2  ,h3+1) 
          Bzm(i,j,k) = ddx*ddz*BzRead(h1  ,h2  ,h3  ) &
                     + ddx* dz*BzRead(h1  ,h2  ,h3+1) &
                     +  dx*ddz*BzRead(h1+1,h2  ,h3  ) &
                     +  dx* dz*BzRead(h1+1,h2  ,h3+1) 
        end if
        if(posx>0. .AND. posx<xmaxB .AND. posy>0. .AND. posy<ymaxB .AND. posz==0.) then
          h1 = posx/deltaxB+1
          h2 = posy/deltayB+1
          h3 = posz/deltazB+1
          dx = posx-(h1-1)*deltaxB
          dy = posy-(h2-1)*deltayB
          ddx = 1.-dx
          ddy = 1.-dy
          Bxm(i,j,k) = ddx*ddy*BxRead(h1  ,h2  ,h3  ) &
                     + ddx* dy*BxRead(h1  ,h2+1,h3  ) &
                     +  dx*ddy*BxRead(h1+1,h2  ,h3  ) &
                     +  dx* dy*BxRead(h1+1,h2+1,h3  ) 
          Bym(i,j,k) = ddx*ddy*ByRead(h1  ,h2  ,h3  ) &
                     + ddx* dy*ByRead(h1  ,h2+1,h3  ) &
                     +  dx*ddy*ByRead(h1+1,h2  ,h3  ) &
                     +  dx* dy*ByRead(h1+1,h2+1,h3  ) 
          Bzm(i,j,k) = ddx*ddy*BzRead(h1  ,h2  ,h3  ) &
                     + ddx* dy*BzRead(h1  ,h2+1,h3  ) &
                     +  dx*ddy*BzRead(h1+1,h2  ,h3  ) &
                     +  dx* dy*BzRead(h1+1,h2+1,h3  ) 
        end if
        if(posx>0. .AND. posx<xmaxB .AND. posy>0. .AND. posy<ymaxB .AND. posz==zmaxB) then
          h1 = posx/deltaxB+1
          h2 = posy/deltayB+1
          h3 = posz/deltazB+1
          dx = posx-(h1-1)*deltaxB
          dy = posy-(h2-1)*deltayB
          ddx = 1.-dx
          ddy = 1.-dy
          Bxm(i,j,k) = ddx*ddy*BxRead(h1  ,h2  ,h3  ) &
                     + ddx* dy*BxRead(h1  ,h2+1,h3  ) &
                     +  dx*ddy*BxRead(h1+1,h2  ,h3  ) &
                     +  dx* dy*BxRead(h1+1,h2+1,h3  ) 
          Bym(i,j,k) = ddx*ddy*ByRead(h1  ,h2  ,h3  ) &
                     + ddx* dy*ByRead(h1  ,h2+1,h3  ) &
                     +  dx*ddy*ByRead(h1+1,h2  ,h3  ) &
                     +  dx* dy*ByRead(h1+1,h2+1,h3  ) 
          Bzm(i,j,k) = ddx*ddy*BzRead(h1  ,h2  ,h3  ) &
                     + ddx* dy*BzRead(h1  ,h2+1,h3  ) &
                     +  dx*ddy*BzRead(h1+1,h2  ,h3  ) &
                     +  dx* dy*BzRead(h1+1,h2+1,h3  ) 
        end if
!end if
      end do
    end do
  end do

  !do i=i0-1, i1+1
  !  do j=j0-1, j1+1
  !    do k=k0-1, k1+1
  !      posx = i*delta(0)
  !      posy = j*delta(1)
  !      posz = k*delta(2)
  !      write(100000+rank, *) i, j, k, Bxm(i,j,k), Bym(i,j,k), Bzm(i,j,k)
  !    end do
  !  end do
  !end do
 

  deallocate(BxRead)
  deallocate(ByRead)
  deallocate(BzRead)
     
end subroutine Read_Bfield_IPP3

subroutine Read_extraction_voltage()
	use Parallel
  implicit none
  include 'mpif.h'
  integer, parameter :: input_unit = 11
  integer  file_status,h1,h2,h3,i,j,k

  character (len=40) FileName
  real :: deltayEG, deltazEG
  integer :: NyEG,NzEG
  real a,b,cc,ddd, ymaxEG, zmaxEG , posx, posy, posz, ddx, ddy, ddz
	

  NyEG=590
  NzEG=590

  allocate(V_EG_Read(1:NyEG, 1:NzEG))
  
  deltayEG=0.000034
  deltazEG=0.000034
  

  ymaxEG = (NyEG-1)*deltayEG
  zmaxEG = (NzEG-1)*deltazEG
     
  FileName="ELISE_Ue"	
  
  h1=0
  h2=0
  h3=0
  i=0
  j=0
  k=0
!***********************************************************************
		!OPEN AND READ INPUT FILE GIVEN BY IBSimu
!***********************************************************************    

  open(unit=input_unit, file=FileName, status="old",iostat=file_status)
  
  if(file_status==0) then  
    do i=1,NyEG
      do j=1, NzEG
          read( unit=input_unit, fmt=* ) a,b,V_EG_Read(i,j)
      end do
    end do
   print *, "file opended and read : ",FileName
    close( unit=input_unit )
  else
   print *, "Unable to open file : ",FileName
  endif

!***********************************************************************
		!SHIFT THE ZERO POSITION AND INTERPOLATE TO
		! THE GRID POSITIONS FOR EACH TASK
!***********************************************************************
  do j=j0-1, j1+1
    do k=k0-1, k1+1
        posy = j*delta(1)
        posz = k*delta(2)
        if(posy>0. .AND. posy<ymaxEG .AND. posz>0. .AND. posz<zmaxEG ) then
          h1 = posy/deltayEG+1
          h2 = posz/deltazEG+1
          
          dy = posy-(h1-1)*deltayEG
          dz = posz-(h2-1)*deltazEG
          
          ddy = 1.-dy
          ddz = 1.-dz
          
          V_EG(j,k) = ddy*ddz*V_EG_Read(h1  ,h2  ) &
                     + ddy*dz*V_EG_Read(h1  ,h2+1) &
                     + dy*ddz*V_EG_Read(h1+1,h2  ) &
                     + dy*dz*V_EG_Read(h1+1,h2+1)
                     
          end if
!if(3==3) then
        if(posy==0.  .AND. posz>0. .AND. posz<ZmaxEG ) then
          h1 = posy/deltayEG+1
          h2 = posz/deltazEG+1
          
          dy = posy-(h1-1)*deltayEG

          dz = posz-(h2-1)*deltazEG
          
          ddy = 1.-dy
          ddz = 1.-dz
          
          V_EG(j,k) = ddz*V_EG_Read(h1  ,h2  ) &
                       + dz*V_EG_Read(h1  ,h2+1) 
        end if
        if(posy==ymaxEG  .AND. posz>0. .AND. posz<ZmaxEG) then
          h1 = posy/deltayEG+1
          h2 = posz/deltazEG+1
          
          dy = posy-(h1-1)*deltayEG
          dz = posz-(h2-1)*deltazEG
          
          ddy = 1.-dy
          ddz = 1.-dz
          
          V_EG(j,k) = ddz*V_EG_Read(h1  ,h2  ) &
                     + dz*V_EG_Read(h1  ,h2+1)
         end if
        if(posy>0 .AND. posy<ymaxEG  .AND. posz==0.) then
          h1 = posy/deltayEG+1
          h2 = posz/deltazEG+1
          
          dy = posy-(h1-1)*deltayEG

          dz = posz-(h2-1)*deltazEG
          
          ddy = 1.-dy
          ddz = 1.-dz
          
          V_EG(j,k) = ddy*V_EG_Read(h1  ,h2  ) &
                     + dy*V_EG_Read(h1+1,h2  )
         end if
        if(posy>0 .AND. posy<ymaxEG  .AND. posz==ZmaxEG) then
          h1 = posy/deltayEG+1
          h2 = posz/deltazEG+1
          
          dy = posy-(h1-1)*deltayEG

          dz = posz-(h2-1)*deltazEG
          
          ddy = 1.-dy
          ddz = 1.-dz
          
          V_EG(j,k) = ddy*V_EG_Read(h1  ,h2  ) &
                     + dy*V_EG_Read(h1+1,h2  ) 
          end if

	  if(posy==0  .AND. posz==0) then
	  	V_EG(0,0) = V_EG_Read(1 , 1  ) 
          end if	



       
    end do
  end do


   deallocate(V_EG_Read)
 
end subroutine Read_extraction_voltage




subroutine Read_Particles_Deuterium()

	use Parallel
	implicit none

  integer :: l
  logical isin
	integer, parameter :: input_unit = 11
	integer  file_status,i,ttt,counter_i
	
	character (len=7) uuu

	
	counter_i=0
	ttt=Iteration_Number+rank

	

	!verify the length of the number in Pqa_pos_NUMBER file and change (i5) (i6) according to the length
	write(uuu,'(i7)') ttt

   lgc=LEN_TRIM(ADJUSTL(uuu))
   open( unit=input_unit, file='Pa_pos_'//uuu(8-lgc:7)//'.dat', status="old",iostat=file_status)

	if(file_status==0) then

		read( unit=input_unit, fmt=* ) Npart

		do i=1,Npart

			read( unit=input_unit, fmt=* ) counter_i, P(i)%X, P(i)%Y, P(i)%Z, P(i)%sp,P(i)%VX,P(i)%VY,P(i)%VZ,P(i)%IdentPart
			if(  P(i)%sp == 1 .OR. P(i)%sp == 2 .OR. P(i)%sp == 3  .OR. P(i)%sp == 4 .OR. P(i)%sp == 5   .OR. P(i)%sp == 8 .OR. P(i)%sp == 9  .OR. P(i)%sp == 10 ) then			
				P(i)%VX=P(i)%VX/(sqrt(2.0))
				P(i)%VY=P(i)%VY/(sqrt(2.0))
				P(i)%VZ=P(i)%VZ/(sqrt(2.0))
			endif			
		enddo

		close( unit=input_unit )

	else
 		print *, "ERROR!!!!  Unable to open file : ",'Pa_pos_'//uuu(8-lgc:7)//'.dat'
        stop
		
	endif
  
  if(3==3) then
  OutParticles = 0
  do l=1, Npart
    isin = .FALSE.
    do ll=1, Nb_Obj
      call isinobj(P(l)%x, P(l)%y, P(l)%z, ll, isin)
      if(isin .EQV. .TRUE.) then
        OutParticles = OutParticles + 1
        DelParticles(OutParticles) = l
        exit
      end if
    end do
  end do

  call Heapsort(DelParticles, OutParticles)
  call DeleteParticle()
  end if


    end subroutine Read_Particles_Deuterium


